import javax.swing.*; 
import java.awt.*;  
import java.awt.event.*;

public class Test extends JFrame implements ActionListener{
    
        JPanel cimiento = (JPanel) this.getContentPane(); 
            //Creacion del formulario (nombre: cimiento), aqui se ponen los botones, pantalla.
        
        JTextField pantalla = new JTextField();
            //Creacion de la pantalla, donde veremos lo que apretamos.
        
        JButton boton1 = new JButton("7");
            //Creacion de los botones.
        JButton boton2 = new JButton("8");
            //Creacion de los botones.
        JButton boton3 = new JButton("9");
            //Creacion de los botones.
        JButton boton4 = new JButton("Borrar");
            //Creacion de los botones.
        JButton boton5 = new JButton("4");
            //Creacion de los botones.
        JButton boton6 = new JButton("5");
            //Creacion de los botones.
        JButton boton7 = new JButton("6");
            //Creacion de los botones.
        JButton boton8 = new JButton("*");
            //Creacion de los botones.
        JButton boton9 = new JButton("1");
            //Creacion de los botones.
        JButton boton10 = new JButton("2");
            //Creacion de los botones.
        JButton boton11 = new JButton("3");
            //Creacion de los botones.
        JButton boton12 = new JButton("/");
            //Creacion de los botones.
        JButton boton13 = new JButton("0");
            //Creacion de los botones.
        JButton boton14 = new JButton(".");
            //Creacion de los botones.
        JButton boton15 = new JButton("-");
            //Creacion de los botones.
        JButton boton16 = new JButton("+");
            //Creacion de los botones.
        JButton boton17 = new JButton("=");
            //Creacion de los botones.
        
        public Test(){ 
            
            //Caracteristicas del cimiento que creamos arriba con JPanel.
            cimiento.setLayout(null);   //con este los botones los podemos modificar.
            setSize(350,450);            //ancho y largo.
            setTitle("Calculadora");   
            setVisible(true);           //no se ve nada por defecto.
            cimiento.setBackground(Color.blue);
            
            //Caracteristicas de la Pantalla.
            pantalla.setBounds(22, 8, 290, 70); //ubicacion X,Y; ancho y alto. 
            pantalla.setFont(new Font("Calibri",Font.BOLD,35)); //caracteristicas de la letra escritas en pantalla
            add(pantalla); //para que se vea la pantalla.
            pantalla.setBackground(Color.lightGray);
            
            //Caracteristicas de los botones.
            boton1.setBounds(22, 83, 60, 60);    //ubicacion X,Y; ancho y alto.
            boton1.setFont(new Font("Calibri",Font.BOLD,30));   //tipo de letra;si es cursiva o etc; tamanio letra.
            add(boton1); //para que se vea el boton.
            boton1.addActionListener(this);
            
            boton2.setBounds(87, 83, 60, 60);    
            boton2.setFont(new Font("Calibri",Font.BOLD,30));  
            add(boton2); 
            boton2.addActionListener(this);
            
            boton3.setBounds(152, 83, 60, 60);  
            boton3.setFont(new Font("Calibri",Font.BOLD,30));   
            add(boton3); //para que se vea el boton
            boton3.addActionListener(this);
            
            boton4.setBounds(217, 83, 95, 60);    
            boton4.setFont(new Font("Calibri",Font.BOLD,20));   
            add(boton4); 
            boton4.addActionListener(this);
            
            boton5.setBounds(22, 148, 60, 60);    
            boton5.setFont(new Font("Calibri",Font.BOLD,30));  
            add(boton5); 
            boton5.addActionListener(this);
            
            boton6.setBounds(87, 148, 60, 60);  
            boton6.setFont(new Font("Calibri",Font.BOLD,30));   
            add(boton6); //para que se vea el boton
            boton6.addActionListener(this);
            
            boton7.setBounds(152, 148, 60, 60);    
            boton7.setFont(new Font("Calibri",Font.BOLD,30));   
            add(boton7); 
            boton7.addActionListener(this);
            
            boton8.setBounds(217, 148, 95, 60);    
            boton8.setFont(new Font("Calibri",Font.BOLD,30));  
            add(boton8); 
            boton8.addActionListener(this);
            
            boton9.setBounds(22, 213, 60, 60);  
            boton9.setFont(new Font("Calibri",Font.BOLD,30));   
            add(boton9); 
            boton9.addActionListener(this);
            
            boton10.setBounds(87, 213, 60, 60);    
            boton10.setFont(new Font("Calibri",Font.BOLD,30));   
            add(boton10); 
            boton10.addActionListener(this);
            
            boton11.setBounds(152, 213, 60, 60);    
            boton11.setFont(new Font("Calibri",Font.BOLD,30));  
            add(boton11); 
            boton11.addActionListener(this);
            
            boton12.setBounds(217, 213, 95, 60);  
            boton12.setFont(new Font("Calibri",Font.BOLD,30));   
            add(boton12); 
            boton12.addActionListener(this);
            
            boton13.setBounds(22, 278, 60, 60);    
            boton13.setFont(new Font("Calibri",Font.BOLD,30));   
            add(boton13); 
            boton13.addActionListener(this);
            
            boton14.setBounds(87, 278, 60, 60);    
            boton14.setFont(new Font("Calibri",Font.BOLD,30));  
            add(boton14); 
            boton14.addActionListener(this);
            
            boton15.setBounds(152, 278, 60, 60);  
            boton15.setFont(new Font("Calibri",Font.BOLD,30));   
            add(boton15); 
            boton15.addActionListener(this);
            
            boton16.setBounds(217, 278, 95, 60);    
            boton16.setFont(new Font("Calibri",Font.BOLD,30));   
            add(boton16); 
            boton16.addActionListener(this);
            
            boton17.setBounds(22, 343, 290, 60);    
            boton17.setFont(new Font("Calibri",Font.BOLD,30));   
            add(boton17); 
            boton17.addActionListener(this);

         }
    
    

public static void main(String[] args) {
        new Test();
     }
    
    
    

public void actionPerformed(ActionEvent e) {    //para el implements, fue autogenerado para quitar error
       
    if(e.getSource()==boton1){          //si el click viene de boton 1 es apretado va a hacer lo que sigue
        if(pantalla.getText().length()== 0){        // length caracteres en una cadena, el alrgo de cadena
             pantalla.setText("7");     //si esta vacia escribe
        }
        else{pantalla.setText(pantalla.getText()+ "7");     //si no esta vacia, escribe lo que tiene en pantalla mas el boton
        }
        boton16.setEnabled(true);
        boton15.setEnabled(true);
        boton12.setEnabled(true);
        boton8.setEnabled(true);
    }
    if(e.getSource()==boton2){
        if(pantalla.getText().length()== 0){
             pantalla.setText("8");
        }
        else{pantalla.setText(pantalla.getText()+ "8");
        }
        boton16.setEnabled(true);
        boton15.setEnabled(true);
        boton12.setEnabled(true);
        boton8.setEnabled(true);
    }
    if(e.getSource()==boton3){          //si el click viene deboton 3 es apretado va a hacer lo que sigue
        if(pantalla.getText().length()== 0){
             pantalla.setText("9");     //si esta vacia escribe
        }
        else{pantalla.setText(pantalla.getText()+ "9");     //si no esta vacia, escribe lo que tiene en pantalla mas el boton
        }
        boton16.setEnabled(true);
        boton15.setEnabled(true);
        boton12.setEnabled(true);
        boton8.setEnabled(true);
    }
    if(e.getSource()==boton4){          //si el click viene deboton 4 es apretado va a hacer lo que sigue
        pantalla.setText("");
        boton14.setEnabled(true);
        boton16.setEnabled(true);
        boton15.setEnabled(true);
        boton12.setEnabled(true);
        boton8.setEnabled(true);
    }
    if(e.getSource()==boton5){
        if(pantalla.getText().length()== 0){
             pantalla.setText("4");
        }
        else{pantalla.setText(pantalla.getText()+ "4");
        }
        boton16.setEnabled(true);
        boton15.setEnabled(true);
        boton12.setEnabled(true);
        boton8.setEnabled(true);
    }
    if(e.getSource()==boton6){
        if(pantalla.getText().length()== 0){        // length caracteres en una cadena, el alrgo de cadena
             pantalla.setText("5");
        }
        else{pantalla.setText(pantalla.getText()+ "5");
        }
        boton16.setEnabled(true);
        boton15.setEnabled(true);
        boton12.setEnabled(true);
        boton8.setEnabled(true);
    }
    if(e.getSource()==boton7){
        if(pantalla.getText().length()== 0){
             pantalla.setText("6");
        }
        else{pantalla.setText(pantalla.getText()+ "6");
        }
        boton16.setEnabled(true);
        boton15.setEnabled(true);
        boton12.setEnabled(true);
        boton8.setEnabled(true);
    }
    if(e.getSource()==boton8){
        if(pantalla.getText().length()== 0){
             pantalla.setText("");
        }
        else{pantalla.setText(pantalla.getText()+ "*");
        boton16.setEnabled(false);
        boton15.setEnabled(false);
        boton12.setEnabled(false);
        boton8.setEnabled(false);
        }
        boton14.setEnabled(true);
    }
    if(e.getSource()==boton9){      //si el click viene deboton 5 es apretado va a hacer lo que sigue
        if(pantalla.getText().length()== 0){        // length caracteres en una cadena, el alrgo de cadena
             pantalla.setText("1");
        }
        else{pantalla.setText(pantalla.getText()+ "1");
        }
        boton16.setEnabled(true);
        boton15.setEnabled(true);
        boton12.setEnabled(true);
        boton8.setEnabled(true);
    }
    if(e.getSource()==boton10){
        if(pantalla.getText().length()== 0){
             pantalla.setText("2");
        }
        else{pantalla.setText(pantalla.getText()+ "2");
        }
        boton16.setEnabled(true);
        boton15.setEnabled(true);
        boton12.setEnabled(true);
        boton8.setEnabled(true);
    }
    if(e.getSource()==boton11){
        if(pantalla.getText().length()== 0){
             pantalla.setText("3");
        }
        else{pantalla.setText(pantalla.getText()+ "3");
        }
        boton16.setEnabled(true);
        boton15.setEnabled(true);
        boton12.setEnabled(true);
        boton8.setEnabled(true);
    }
    if(e.getSource()==boton12){
        if(pantalla.getText().length()== 0){
             pantalla.setText("");
        }
        else{pantalla.setText(pantalla.getText()+ "/");
        boton16.setEnabled(false);
        boton15.setEnabled(false);
        boton12.setEnabled(false);
        boton8.setEnabled(false);
        }
        boton14.setEnabled(true);
    }
    
    if(e.getSource()==boton13){     //si el click viene deboton 13 es apretado va a hacer lo que sigue
        if(pantalla.getText().length()== 0){        // length caracteres en una cadena, el alrgo de cadena
             pantalla.setText("0");
        }
        else{pantalla.setText(pantalla.getText()+ "0");
        }
        boton16.setEnabled(true);
        boton15.setEnabled(true);
        boton12.setEnabled(true);
        boton8.setEnabled(true);
    }
    if(e.getSource()==boton14){
        if(pantalla.getText().length()== 0){
             pantalla.setText(".");
        }
        else{pantalla.setText(pantalla.getText()+ ".");
        }
        boton14.setEnabled(false);
    }
    if(e.getSource()==boton15){
        if(pantalla.getText().length()== 0){
             pantalla.setText("");
        }
        else{pantalla.setText(pantalla.getText()+ "-");
        boton16.setEnabled(false);
        boton15.setEnabled(false);
        boton12.setEnabled(false);
        boton8.setEnabled(false);
        }
        boton14.setEnabled(true);
    }
    if(e.getSource()==boton16){
        if(pantalla.getText().length()== 0){
             pantalla.setText("");
        }
        else{pantalla.setText(pantalla.getText()+ "+");
        boton16.setEnabled(false);
        boton15.setEnabled(false);
        boton12.setEnabled(false);
        boton8.setEnabled(false);
        }
        boton14.setEnabled(true);
    }
    if(e.getSource()==boton17){
        String valoresPantalla = pantalla.getText();  //pasamos lo que hay en pantalla a largo de cadena
        
            for(int i=0;i<valoresPantalla.length();i++){    //recorremos toda la cadena          
                char temp=valoresPantalla.charAt(i);    //pasamos la posicion del String al valor que tiene esa posicion
                    
                    if(temp=='+'){
                        String primeraParte=valoresPantalla.substring(0,i); //pasamos a numeros la cadena antes del signo
                        String segundaParte=valoresPantalla.substring(i+1,valoresPantalla.length());//pasamos a numeros la cadena despues del signo
                        double resultado= Double.parseDouble(primeraParte)+Double.parseDouble(segundaParte);
                        
                        pantalla.setText(Double.toString(resultado)); //imprimimos el resultado en pantalla pasandolo a string otra vez
                    }    
                    
                    if(temp=='-'){
                        String primeraParte=valoresPantalla.substring(0,i); //pasamos a numeros la cadena antes del signo
                        String segundaParte=valoresPantalla.substring(i+1,valoresPantalla.length());//pasamos a numeros la cadena despues del signo
                        double resultado= Double.parseDouble(primeraParte)-Double.parseDouble(segundaParte);
                        
                        pantalla.setText(Double.toString(resultado)); //imprimimos el resultado en pantalla pasandolo a string otra vez
                    }
                    
                    if(temp=='*'){
                        String primeraParte=valoresPantalla.substring(0,i); //pasamos a numeros la cadena antes del signo
                        String segundaParte=valoresPantalla.substring(i+1,valoresPantalla.length());//pasamos a numeros la cadena despues del signo
                        double resultado= Double.parseDouble(primeraParte)*Double.parseDouble(segundaParte);
                        
                        pantalla.setText(Double.toString(resultado)); //imprimimos el resultado en pantalla pasandolo a string otra vez
                    }
                    
                    if(temp=='/'){
                        String primeraParte=valoresPantalla.substring(0,i); //(sub para sacar mitad a cadena)pasamos a numeros la cadena antes del signo
                        String segundaParte=valoresPantalla.substring(i+1,valoresPantalla.length());//pasamos a numeros la cadena despues del signo
                        double resultado= Double.parseDouble(primeraParte)/Double.parseDouble(segundaParte);
                        
                        if(Double.parseDouble(segundaParte)==0){pantalla.setText("No se puede dividir entre 0");
                        }
                        else{
                        pantalla.setText(Double.toString(resultado)); //imprimimos el resultado en pantalla pasandolo a string otra vez
                        }
                    }
            }
            boton14.setEnabled(true);
    }
        
    }
    
    
         



}